package com.dtorres.gossiptest.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Test;

import com.dtorres.gossiptest.config.GossipTestCase;
import com.dtorres.gossiptest.config.Properties;
import com.dtorres.gossiptest.model.Gossip;
import com.dtorres.gossiptest.model.Person;
import com.dtorres.gossiptest.model.Store;

public class GossipServiceImplTest {

    private Properties properties      = mock(Properties.class);
    private GossipServiceImpl instance = new GossipServiceImpl(properties);
    
    @Test
    public void numberStopsGossipProblemTest(){
        
        when(properties.getDurationToGoStore()).thenReturn(1);
        when(properties.getTimeToBuy()).thenReturn(480);
        
        assertEquals("Stops number is wrong!", 
                instance.numberStopsGossipProblem(this.loadPersonsStores(GossipTestCase.GOSSIP_TEST_CASE_0)), 
                new Integer(0));
        
        assertEquals("Stops number is wrong!", 
                instance.numberStopsGossipProblem(this.loadPersonsStores(GossipTestCase.GOSSIP_TEST_CASE_5)), 
                new Integer(5));
        
        assertEquals("Stops number is wrong!", 
                instance.numberStopsGossipProblem(this.loadPersonsStores(GossipTestCase.GOSSIP_TEST_CASE_13)), 
                new Integer(13));

        assertEquals("Stops number is wrong!", 
                instance.numberStopsGossipProblem(this.loadPersonsStores(GossipTestCase.GOSSIP_TEST_CASE_24)), 
                new Integer(24));
        
        assertEquals("Stops number is wrong!", 
                instance.numberStopsGossipProblem(this.loadPersonsStores(GossipTestCase.GOSSIP_TEST_CASE_52)), 
                new Integer(52));
        
        // Increment duration to go store
        when(properties.getDurationToGoStore()).thenReturn(10);
        when(properties.getTimeToBuy()).thenReturn(480);
        
        assertEquals("Stops number is wrong!", 
                instance.numberStopsGossipProblem(this.loadPersonsStores(GossipTestCase.GOSSIP_TEST_CASE_24)), 
                new Integer(24));
        
        assertEquals("Stops number is wrong!", 
                instance.numberStopsGossipProblem(this.loadPersonsStores(GossipTestCase.GOSSIP_TEST_CASE_52)), 
                new Integer(0));
    }

    @Test
    public void exchangeGossipTest(){
        
        List<Person> persons = this.loadPersonsStores(GossipTestCase.GOSSIP_TEST_CASE_5);
        instance.exchangeGossip(persons, 0);
        assertEquals("Exchange gossip wrong!", persons.get(0).getGossips().size(), 2);
        assertEquals("Exchange gossip wrong!", persons.get(1).getGossips().size(), 2);
        assertEquals("Exchange gossip wrong!", persons.get(2).getGossips().size(), 1);
        
        persons = this.loadPersonsStores(GossipTestCase.GOSSIP_TEST_CASE_52);
        instance.exchangeGossip(persons, 10);
        assertEquals("Exchange gossip wrong!", persons.get(0).getGossips().size(), 1);
        assertEquals("Exchange gossip wrong!", persons.get(1).getGossips().size(), 1);
        assertEquals("Exchange gossip wrong!", persons.get(2).getGossips().size(), 1);
        assertEquals("Exchange gossip wrong!", persons.get(3).getGossips().size(), 1);
        
        persons = this.loadPersonsStores(GossipTestCase.GOSSIP_TEST_CASE_52);
        instance.exchangeGossip(persons, 17);
        assertEquals("Exchange gossip wrong!", persons.get(0).getGossips().size(), 2);
        assertEquals("Exchange gossip wrong!", persons.get(1).getGossips().size(), 1);
        assertEquals("Exchange gossip wrong!", persons.get(2).getGossips().size(), 2);
        assertEquals("Exchange gossip wrong!", persons.get(3).getGossips().size(), 1);
    }
    
    @Test
    public void canExchangeGossipTest(){
        Person person1 = this.loadPerson("2 3 2 1");
        Person person2 = this.loadPerson("1 2 3 1");
        assertFalse("Persons can exchange gossip!",   instance.canExchangeGossip(person1, person2, 0));
        assertTrue ("Persons can't exchange gossip!", instance.canExchangeGossip(person1, person2, 3));
        assertTrue ("Persons can't exchange gossip!", instance.canExchangeGossip(person1, person2, 7));
        assertFalse("Persons can't exchange gossip!", instance.canExchangeGossip(person1, person2, 4));
        
        Person person3 = this.loadPerson("1 2 4 5 7");
        Person person4 = this.loadPerson("7 2 3");
        assertFalse("Persons are in same store!",    instance.canExchangeGossip(person3, person4, 0));
        assertTrue ("Persons aren't in same store!", instance.canExchangeGossip(person3, person4, 9));
        assertFalse("Persons are in same store!",    instance.canExchangeGossip(person3, person4, 13));
    }

    @Test
    public void allPersonKnowsGossipTest(){
        List<Person> persons = new ArrayList<Person>();
        
        persons.add(this.loadPerson(""));
        persons.add(this.loadPerson(""));
        persons.add(this.loadPerson(""));
        assertFalse("Persons knows all gossip!", instance.allPersonKnowsGossip(persons));
        
        persons.get(0).getGossips().addAll(persons.get(1).getGossips());
        persons.get(1).getGossips().addAll(persons.get(0).getGossips());
        assertFalse("Persons knows all gossip!", instance.allPersonKnowsGossip(persons));
        
        persons.get(2).getGossips().addAll(persons.get(1).getGossips());
        persons.get(1).getGossips().addAll(persons.get(2).getGossips());
        assertFalse("Persons knows all gossip!", instance.allPersonKnowsGossip(persons));
        
        persons.get(2).getGossips().addAll(persons.get(0).getGossips());
        persons.get(0).getGossips().addAll(persons.get(2).getGossips());
        assertTrue("Persons don't knows all gossip!", instance.allPersonKnowsGossip(persons));
    }

    private List<Person> loadPersonsStores(String[] stores){
        List<Person> result = new ArrayList<Person>();
        Stream.of(stores).forEach(e -> result.add(this.loadPerson(e)));
        return result;
    }
    
    private Person loadPerson(String stores){
        Person person = new Person();
        List<Store> storesList = new ArrayList<Store>();
        Arrays.asList(stores.split(StoreServiceImpl.STORE_SPLIT_REGEX)).stream().forEach(e -> storesList.add(new Store(e)));
        person.setStores(storesList);
        person.setGossips(new HashSet<Gossip>(Arrays.asList(new Gossip())));
        return person;
    }
}
