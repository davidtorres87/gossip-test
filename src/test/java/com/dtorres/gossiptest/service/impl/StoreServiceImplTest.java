package com.dtorres.gossiptest.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.dtorres.gossiptest.model.Store;

public class StoreServiceImplTest {
    
    private StoreServiceImpl instance = new StoreServiceImpl();
    
    @Test
    public void initTest(){
        List<Store> list = instance.load("2 3 1 2 8");
        
        assertEquals("Store list size wrong!", list.size(), 5);
        assertEquals("Store index wrong!",     list.get(0).getCode(), "2");
        assertEquals("Store index wrong!",     list.get(2).getCode(), "1");
        assertEquals("Store index wrong!",     list.get(4).getCode(), "8");
    }
}
