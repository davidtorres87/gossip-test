package com.dtorres.gossiptest.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Test;

import com.dtorres.gossiptest.model.Person;
import com.dtorres.gossiptest.model.Store;
import com.dtorres.gossiptest.service.StoreService;

public class PersonServiceImplTest {
    
    private StoreService storeService  = mock(StoreService.class);
    private PersonServiceImpl instance = new PersonServiceImpl(storeService);
    
    @Test
    public void loadTest(){
        String stores  = "2 3 1 2 8";
        InputStream in = new ByteArrayInputStream(stores.getBytes());
        when(storeService.load(anyString())).thenReturn(loadStores(stores));
        
        List<Person> persons = instance.load(in);
        
        assertEquals("Load persons is wrong!", persons.size(), 1);
        assertEquals("Load persons is wrong!", persons.get(0).getStores().size(), 5);
        assertEquals("Load persons is wrong!", persons.get(0).getStores().get(0).getCode(), "2");
        assertEquals("Load persons is wrong!", persons.get(0).getStores().get(2).getCode(), "1");
        assertEquals("Load persons is wrong!", persons.get(0).getStores().get(4).getCode(), "8");
        assertEquals("Load persons is wrong!", persons.get(0).getGossips().size(), 1);
        
        String stores2  = "6 2 4 7 5 4 2";
        in = new ByteArrayInputStream(stores.concat("\n").concat(stores2).getBytes());
        when(storeService.load(stores)).thenReturn(loadStores(stores));
        when(storeService.load(stores2)).thenReturn(loadStores(stores2));
        persons = instance.load(in);
        
        assertEquals("Load persons is wrong!", persons.size(), 2);
        assertEquals("Load persons is wrong!", persons.get(1).getStores().size(), 7);
        assertEquals("Load persons is wrong!", persons.get(1).getStores().get(0).getCode(), "6");
        assertEquals("Load persons is wrong!", persons.get(1).getStores().get(2).getCode(), "4");
        assertEquals("Load persons is wrong!", persons.get(1).getStores().get(6).getCode(), "2");
        assertEquals("Load persons is wrong!", persons.get(1).getGossips().size(), 1);
    }
    
    @Test
    public void loadPersonTest(){
        String stores  = "2 3 1 2 8";
        when(storeService.load(anyString())).thenReturn(loadStores(stores));
        Person person = instance.loadPerson(stores);
        
        assertEquals("Load person is wrong!", person.getStores().size(), 5);
        assertEquals("Load person is wrong!", person.getStores().get(0).getCode(), "2");
        assertEquals("Load person is wrong!", person.getStores().get(2).getCode(), "1");
        assertEquals("Load person is wrong!", person.getStores().get(4).getCode(), "8");
        assertEquals("Load person is wrong!", person.getGossips().size(), 1);
    }
    
    private List<Store> loadStores(String stores){
        List<Store> result = new ArrayList<Store>();
        Stream.of(stores.split(StoreServiceImpl.STORE_SPLIT_REGEX)).forEach(e -> result.add(new Store(e)));
        return result;
    }
}
