package com.dtorres.gossiptest.config;

public final class GossipTestCase {
    
    public static final String[] GOSSIP_TEST_CASE_0  = {"2 1 2",
                                                        "5 2 8"};

    public static final String[] GOSSIP_TEST_CASE_5  = {"3 1 2 3",
                                                        "3 2 3 1",
                                                        "4 2 3 4 5"};

    public static final String[] GOSSIP_TEST_CASE_13 = {"3 1 2 3 4 6 3 5 2 4 6 2",
                                                        "3 2 3 1 3 4 5 6 2",
                                                        "4 5 3 4 5 6 4 5",
                                                        "1 4 3 3 5 6 4 5 3 2",
                                                        "6 6 5 2 4",
                                                        "5 2 6 3 2 3 4 2 1 3",
                                                        "3 4 2 1 3 1 2",
                                                        "2 3 4 5 6 2 1 1"};

    public static final String[] GOSSIP_TEST_CASE_24 = {"8 2 3 7 5 4 12 14 15 2 3",
                                                        "11 13 4 6 10 9 11 4 3 5 1 5",
                                                        "15 6 5 4 7 8 13 12",
                                                        "9 1 8 3 2 13 1 15 9 10",
                                                        "1 4 1 11 12 15 2 3 5",
                                                        "2 3 12 8 3 12 15 13",
                                                        "5 5 8 6 11 13"};

    public static final String[] GOSSIP_TEST_CASE_52 = {"20 18 12 4 5 2",
                                                        "10 11 7 9 19 3",
                                                        "17 13 2 3 20",
                                                        "14 9 8 6 8"};

}
