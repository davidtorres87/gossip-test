package com.dtorres.gossiptest.model;

public abstract class MasterEntity {

	private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
