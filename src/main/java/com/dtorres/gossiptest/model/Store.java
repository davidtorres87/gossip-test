package com.dtorres.gossiptest.model;

public final class Store extends MasterEntity {
    
    private String code;

    public Store(String code){
        this.code = code;
    }
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
