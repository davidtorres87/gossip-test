package com.dtorres.gossiptest.model;

import java.util.List;
import java.util.Set;

public final class Person extends MasterEntity {

    private List<Store> stores;
    private Set<Gossip> gossips;

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

    public Set<Gossip> getGossips() {
        return gossips;
    }

    public void setGossips(Set<Gossip> gossips) {
        this.gossips = gossips;
    }

}
