package com.dtorres.gossiptest.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.dtorres.gossiptest.model.Person;
import com.dtorres.gossiptest.service.GossipService;
import com.dtorres.gossiptest.service.PersonService;
import com.dtorres.gossiptest.utils.GossipTestUtils;

@Controller
public class GossipController {

    private static final String PATH_GOSSIPTEST         = "/gossiptest";
    private static final String PATH_GOSSIPTEST_RESOLVE = "/gossiptest/resolve";
    private static final String VIEW_GOSSIP             = "gossip";
    private static final String PARAM_FILE              = "file";
    private static final String ATTRIBUTE_TEST_CASE     = "testCase";
    private static final String ATTRIBUTE_SOLUTION      = "solution";
    
    private PersonService personService;
    private GossipService gossipService;
    
    @Autowired
    public GossipController(PersonService personService, 
                            GossipService gossipService){
        this.personService = personService;
        this.gossipService = gossipService;
    }
    
    @GetMapping(PATH_GOSSIPTEST)
    public String gossip(Model model) {
        return VIEW_GOSSIP;
    }
    
    @PostMapping(PATH_GOSSIPTEST_RESOLVE)
    public String resolve(@RequestParam(PARAM_FILE) MultipartFile file, Model model) throws IOException {
        if(!file.isEmpty()){
            List<Person> personsStores = personService.load(file.getInputStream());
            model.addAttribute(ATTRIBUTE_TEST_CASE, GossipTestUtils.inputStreamToString(file.getInputStream()));
            model.addAttribute(ATTRIBUTE_SOLUTION, gossipService.numberStopsGossipProblem(personsStores));    
        }
        return VIEW_GOSSIP;
    }
    
}
