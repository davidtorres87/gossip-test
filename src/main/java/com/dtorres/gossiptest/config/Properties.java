package com.dtorres.gossiptest.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "gossiptest")
public class Properties {

    private Integer durationToGoStore;
    private Integer timeToBuy;
    
    public Integer getDurationToGoStore() {
        return durationToGoStore;
    }
    public void setDurationToGoStore(Integer durationToGoStore) {
        this.durationToGoStore = durationToGoStore;
    }
    public Integer getTimeToBuy() {
        return timeToBuy;
    }
    public void setTimeToBuy(Integer timeToBuy) {
        this.timeToBuy = timeToBuy;
    }

}
