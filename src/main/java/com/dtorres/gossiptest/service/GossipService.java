package com.dtorres.gossiptest.service;

import java.util.List;

import com.dtorres.gossiptest.model.Person;

public interface GossipService {

    /**
     * Get the number of stops it takes to have all the people on board with the latest gossips
     * @param persons List of persons
     * @return numberStops
     */
    public Integer numberStopsGossipProblem(List<Person> persons);

}
