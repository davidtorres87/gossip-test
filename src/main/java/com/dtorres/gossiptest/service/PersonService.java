package com.dtorres.gossiptest.service;

import java.io.InputStream;
import java.util.List;

import com.dtorres.gossiptest.model.Person;

public interface PersonService {
    
    /**
     * Load a person list from InputStream (Routes File)
     * @param in routes File
     * @return persons
     */
    public List<Person> load(InputStream in);

}
