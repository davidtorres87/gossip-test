package com.dtorres.gossiptest.service;

import java.util.List;

import com.dtorres.gossiptest.model.Store;

public interface StoreService {
    
    /**
     * Load stores from string
     * @param stores String with stores
     * @return stores
     */
    public List<Store> load(String stores);

}
