package com.dtorres.gossiptest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtorres.gossiptest.config.Properties;
import com.dtorres.gossiptest.model.Person;
import com.dtorres.gossiptest.service.GossipService;

@Service
public class GossipServiceImpl implements GossipService {

    public Properties properties;    
    
    @Autowired
    public GossipServiceImpl(Properties properties){
        this.properties = properties;
    }

    @Override
    public Integer numberStopsGossipProblem(List<Person> persons) {
        Integer result = 0;
        for(Integer storeStop = 0; storeStop < properties.getTimeToBuy() / properties.getDurationToGoStore(); storeStop++){
            
            this.exchangeGossip(persons, storeStop);
            
            if(this.allPersonKnowsGossip(persons)){
                result = storeStop + 1;
                break;
            }
        }
        return result;
    }

    /**
     * Exchange gossip between persons (people exchange gossip when they are at the same store)
     * @param persons List of person
     * @param storeStop Store stop number
     */
    protected void exchangeGossip(List<Person> persons, Integer storeStop){
        for(Person person1 : persons){
            for(Person person2 : persons){
                if(this.canExchangeGossip(person1, person2, storeStop)){
                    person1.getGossips().addAll(person2.getGossips());
                    person2.getGossips().addAll(person1.getGossips());
                }
            }
        }
    }
    
    /**
     * Check if two persons can exchange theirs gossip (if they are they in same store)
     * @param person1 Person 1
     * @param person2 Person 2
     * @param storeStop Store stop number
     * @return canExchangeGossip?
     */
    protected Boolean canExchangeGossip(Person person1, Person person2, Integer storeStop){
        return person1.getStores().get(storeStop % person1.getStores().size()).getCode()
                .equals(person2.getStores().get(storeStop % person2.getStores().size()).getCode());
    }
    
    /**
     * Check if all persons knows all gossip
     * @param persons List of person
     * @return allPersonKnowsGossip?
     */
    protected Boolean allPersonKnowsGossip(List<Person> persons){
        Integer numberPersonsKnowsGossip = 0;
        for(Person person : persons){
            if(persons.size() == person.getGossips().size()){
                numberPersonsKnowsGossip++;
            }
        }
        return numberPersonsKnowsGossip == persons.size();
    }

}
