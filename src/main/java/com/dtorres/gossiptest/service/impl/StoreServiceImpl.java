package com.dtorres.gossiptest.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.dtorres.gossiptest.model.Store;
import com.dtorres.gossiptest.service.StoreService;

@Service
public class StoreServiceImpl implements StoreService {

    protected static final String STORE_SPLIT_REGEX = " ";
    
    @Override
    public List<Store> load(String stores) {
        List<Store> result = new ArrayList<Store>();
        Stream.of(stores.split(STORE_SPLIT_REGEX)).forEach(e -> result.add(new Store(e)));
        return result;
    }
    
}
