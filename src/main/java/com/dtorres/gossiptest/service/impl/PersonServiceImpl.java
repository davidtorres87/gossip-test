package com.dtorres.gossiptest.service.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dtorres.gossiptest.model.Gossip;
import com.dtorres.gossiptest.model.Person;
import com.dtorres.gossiptest.service.PersonService;
import com.dtorres.gossiptest.service.StoreService;

@Service
public class PersonServiceImpl implements PersonService {
    
    private StoreService storeService;
    
    @Autowired
    public PersonServiceImpl(StoreService storeService){
        this.storeService = storeService;
    }
    
    @Override
    public List<Person> load(InputStream in) {
        List<Person> result = new ArrayList<Person>();
        new BufferedReader(new InputStreamReader(in)).lines()
                            .filter(line -> !line.isEmpty())
                            .forEach(line -> result.add(this.loadPerson(line)));
        return result;
    }

    protected Person loadPerson(String stores){
        Person person = new Person();
        person.setStores(storeService.load(stores));
        person.setGossips(new HashSet<Gossip>(Arrays.asList(new Gossip())));
        return person;
    }
    
}
