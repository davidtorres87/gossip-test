package com.dtorres.gossiptest.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/**
 * APP Utils
 */
public final class GossipTestUtils {

    private GossipTestUtils(){}

    /**
     * Get String from InputStream
     * @param in InputStream
     * @return string
     */
    public static String inputStreamToString(InputStream in){
        return new BufferedReader(new InputStreamReader(in)).lines().collect(Collectors.joining("\n"));
    }

}
