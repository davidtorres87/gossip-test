package com.dtorres.gossiptest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GossipTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GossipTestApplication.class, args);
	}
}
